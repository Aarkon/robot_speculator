# Provides logic to deal with the cex-api.

require_relative 'cex_api'

class Transaction

	attr_reader :criteria
  attr_accessor :margin
    def initialize
		@cex = CexAPI.new
		@criteria = {}

    # A factor to modify check-values for buying and selling.
    # Helpful to avoid risks of sudden rate changes that happen faster than the
    # completion of a transaction.
    @margin = 0.9965

		refresh_criteria
	end

  def sell?
    price = @criteria["btceur"]
    if price > @criteria["last_buy_price"]
      amount = @criteria["btc_available"] #/ 2
      return self.get_gain("sell") * @margin > ((amount * price) * (@criteria["sell_fee"] / 100))
    end
    return false
  end

  def buy?
    price = @criteria["btceur"]
    if price < @criteria["last_sell_price"]
      amount = @criteria["eur_available"]
      return self.get_gain("buy") * @margin > (amount * (@criteria["sell_fee"] / 100))
    end
    return false
  end
  
  def sell_safe?
    min_btc_met = @criteria["minimum_btc"] < @criteria["btc_available"]
    min_eur_met = @criteria["minimum_eur"] < @criteria["btc_available"] * @criteria["btceur"]
    if self.ready?("sell") && min_btc_met && min_eur_met
      price = @criteria["btceur"]
      return price > (1 / @margin) * @criteria["last_buy_price"]
    end
    return false
  end
  
  def buy_safe?
    min_btc_met = @criteria["minimum_btc"] < @criteria["btceur"] / @criteria["eur_available"]
    min_eur_met = @criteria["minimum_btc"] < @criteria["eur_available"]
    if self.ready?("buy") && min_btc_met && min_eur_met
      price = @criteria["btceur"]
      return price < @criteria["last_sell_price"] * @margin
    end
    return false
  end

  def sell(amount)
    @cex.place_instant_order("sell", amount, couple = "BTC/EUR")
  end

  def buy(amount)
    @cex.place_instant_order("buy", amount, couple = "BTC/EUR")
  end

  def get_gain(action = "sell")
    # How would selling BTC modify the overall balance?
    if action == "sell"
      old = @criteria["btc_available"] * @criteria["last_buy_price"]
      new = @criteria["btc_available"] * @criteria["btceur"]
      return new - old  # Returns gain in Euro
    elsif action == "buy"
      old = @criteria["eur_available"] / @criteria["last_sell_price"]
      new = @criteria["eur_available"] / @criteria["btceur"]
      return (new - old) * @criteria["btceur"]  # Returns gain in Euro
    end
  end

  def sum_up
    sum = @criteria["btc_available"] * @criteria["btceur"]
    sum += @criteria["eur_available"]
    if @criteria["orders"] != []
      @criteria["orders"].each do |order|
        sum += order["pending"].to_f * @criteria["btceur"]
      end
    end
    return sum
  end

  def ready?(action)
    ready = true
    return ready if @criteria["orders"] == []
    @criteria["orders"].each do |order|
      ready = false if order["type"]== action
    end
    ready
  end

  def cancel_if_too_old
    return false if @criteria["orders"] == []
    acceptable = 1000 * 60 * @order_length
    @criteria["orders"].each do |order|
      age = (Time.now.to_i * 1000) - order["time"].to_i
      if age > acceptable
        age = (age / 1000) / 60
        log("#{Time.now}: Cancelling order #{order["id"]}, which is over #{age} minutes old...")
        reply = @cex.cancel_order(order["id"].to_i)
      end
    end
  end

  def get_last_price(orders, type, n = 0)
    if orders[n]["type"] == type
      price = 1 / (orders[n]["a:BTC:cds"].to_f / orders[n]["a:EUR:cds"].to_f)
      return price
    else
      get_last_price(orders, type, n + 1)
    end
  end

  def refresh_criteria
    boundaries = @cex.currency_boundaries
    @criteria["minimum_btc"] = boundaries["data"]["pairs"][1]["minLotSize"].to_f
    @criteria["minimum_eur"] = boundaries["data"]["pairs"][1]["minLotSizeS2"].to_f
    orders = @cex.archived_orders
    @criteria["last_buy_price"] = get_last_price(orders, "buy")
    @criteria["last_sell_price"] = get_last_price(orders, "sell")
    reply = @cex.balance
    @criteria["btc_available"] = reply["BTC"]["available"].to_f
    @criteria["eur_available"] = reply["EUR"]["available"].to_f
    reply = @cex.get_myfee
    @criteria["sell_fee"] = reply["data"]["BTC:EUR"]["sell"].to_f
    @criteria["buy_fee"] = reply["data"]["BTC:EUR"]["buy"].to_f
    @criteria["btceur"] = @cex.last_price("BTC/EUR").to_f
    @criteria["orders"] = @cex.open_orders
    # retries to connect if the connection attempt fails
    rescue NoMethodError
      retry if @criteria.length < 10
      retry if orders == nil
	end
end
