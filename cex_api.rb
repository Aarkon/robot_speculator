#  cex_api.rb
#
#  Mostly based on work by beli-sk (https://github.com/beli-sk) and especially this,
#  which I basically rewrote in ruby:
#  https://github.com/matveyco/cex.io-api-python/blob/master/cexapi/cexapi.py
#  I added several methods.
#  Feel free to donate over there and/or to me, for which this would be the correct address:
#  1G2d6gwbEPh27G72ZadcFRZav6PzcHsM8T

require 'net/http'
require 'json'
require 'openssl'
require 'uri'

class CexAPI
  def identification(item)
    # item can be userID, key or secret.
    JSON.parse(IO.read('api_secret.json'))["#{item}"]
  end

  def api_call(method, param = {}, private = false, couple = nil)
    address = "https://cex.io/api/#{method}/"
    address = address + couple if couple
    if private
      secret = {
        "key" => self.identification("key"),
        "signature" => self.signature,
        "nonce"=> self.nonce}
        param.merge!(secret)
    end
    answer = self.post(address, param)
    return answer if answer.body == "true"
    JSON.parse(answer.body) if answer.code == "200"
  end

  def last_price(couple = "BTC/EUR")
    return 1.0/((self.last_price).to_f) if couple == "BTC/LTC"
    self.api_call("last_price", {}, false, couple)["lprice"]
  end

  def nonce
    Time.now.to_i
  end

  def signature
	  sleep 1
    nonce = self.nonce.to_s
    string = nonce + self.identification("userID") + self.identification("key")
    digest = OpenSSL::Digest.new('sha256')
    signature = OpenSSL::HMAC.hexdigest(digest, self.identification("secret"), string).upcase
  end

  def post(address, params)
    address = URI(address)
    http = Net::HTTP.new address.host, address.port
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    request = Net::HTTP::Post.new(address.request_uri)
    request.form_data = params
    http.start
    response = http.request request
  end

  def ticker(couple="BTC/EUR")
    self.api_call("ticker", {}, false, couple)
  end

  def balance
    self.api_call('balance', {}, true)
  end

  def get_myfee
    self.api_call("get_myfee", {}, true)
  end

  def open_orders(couple = "BTC/EUR")
    self.api_call("open_orders", {}, true, couple)
  end

  def archived_orders(couple = "BTC/EUR", limit = 100, dateTo = (Time.now).strftime("%D"), dateFrom = (Time.now - 24 * 3600).strftime("%D"))
    self.api_call("archived_orders", {"limit" => limit, "dateTo" => dateTo, "dateFrom" => dateFrom}, true, couple)
  end

  def currency_boundaries
    self.api_call("currency_boundaries",{}, true)
  end

  def place_order(ptype, amount, price, couple = "BTC/EUR")
    self.api_call("place_order", {"type" => ptype, "amount" => amount.to_s, "price" => price.to_s}, true, couple)
  end
  
  def place_instant_order(ptype, amount, couple = "BTC/EUR")
    self.api_call("place_order", {"type" => ptype, "amount" => amount.to_s, "order_type" => "market"}, true, couple)
  end

  def cancel_order(order_id)
    self.api_call("cancel_order", {"id" => order_id}, true)
  end
  
  def get_order(id)
    self.api_call("get_order", {"id" => id}, true)
  end
end
