#~ require_relative 'cex_api'
require_relative 'transaction'

class Interaction
	def initialize
		@ta = Transaction.new
	end

  def updates
    loop do
      def update
        @ta.refresh_criteria
        puts Time.now
        puts @ta.criteria
        fee = (@ta.criteria["eur_available"] ) * (@ta.criteria["buy_fee"] / 100)
        price = @ta.criteria["btceur"]
        puts "Buy? #{@ta.buy_safe?}: #{@ta.get_gain("buy")}, #{fee}"
        puts "#{price} must be lower than #{@ta.criteria["last_sell_price"] * @ta.margin**2}"
        fee = ((@ta.criteria["btc_available"]) * @ta.criteria["btceur"]) * (@ta.criteria["sell_fee"] / 100)
        puts "Sell? #{@ta.sell_safe?}: #{@ta.get_gain("sell")}, #{fee}"
        puts "#{price} must be higher than #{@ta.criteria["last_buy_price"] / @ta.margin}"
        p @ta.sum_up
      end
      time = Time.now.to_i
      update
      time = Time.now.to_i - time
      puts "Updating took #{time} seconds."
      time = 60 if time > 60
      sleep 60 - time
      puts
    end
  end

  def buy
    budget = @ta.criteria["eur_available"]
    if budget < @ta.criteria["minimum_eur"]
      puts "Sorry, can't buy enough BTC for #{budget} EUR."
    else
      price = @ta.criteria["btceur"]
      amount =  budget / price
      last_price = @ta.criteria["last_sell_price"]
	  actual_price = (@ta.criteria["buy_fee"] / 100) * price + price
      puts "Last time you sold BTC, they'd cost #{last_price} EUR."
      puts "Spend #{budget} to buy #{amount} BTC for #{price} EUR  (or with fee #{actual_price} EUR) each?"
      puts "[Y] = yes"

      if gets.chomp == "Y"
        puts @ta.buy(budget)
      else
        puts "Aborting..."
      end
    end
  end

  def sell
    budget = @ta.criteria["btc_available"]
    if budget < @ta.criteria["minimum_btc"]
      puts "Sorry, #{budget} BTC isn't enough for selling."
    else
      price = @ta.criteria["btceur"]
      amount = @ta.criteria["btc_available"]
      last_price = @ta.criteria["last_buy_price"]
	    actual_price = price - (price * (@ta.criteria["sell_fee"] / 100))
      puts "You bought the #{amount} BTC for #{last_price}"
      puts "Sell #{amount} BTC for #{price}?"
      puts "[Y] = yes"

      if gets.chomp == "Y"
        puts @ta.sell(amount)
      else
        puts "Aborting..."
      end
    end
  end
end

def main_loop
  puts "What you wanna do?"
  puts "[1] See information on your account, updatet once a minute"
  puts "[2] Buy BTC manually"
  puts "[3] Sell BTC manually"
  puts "[0] Quit"
  choice = gets.chomp

  @interaction = Interaction.new unless choice == "0"
  case choice
  when "1"
    @interaction.updates
  when "2"
    @interaction.buy
    puts
  when "3"
    @interaction.sell
    puts
  when "0"
    exit
  else
    puts "Didn't get what you said."
    main_loop
  end
end

loop do
  main_loop
end
