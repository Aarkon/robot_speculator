#  logger.rb
#  
#  Copyright 2016 Jakob Ledig <jakob.ledig@haw-hamburg.de>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#

class Logger
  include Singleton
  
    def initialize(file = "log.txt")
      @log = file
    end
  
  def replace_line(n = 0, line)
    old = File.new("#{@log}", 'r')
    lines = []
    old.each_line {|line| lines.push(line)}
    old.close
    lines[n] = line
    log = File.open("#{@log}", 'w')  {|file| file.truncate(0)}
    log = File.new("#{@log}", 'a')
    lines.each {|line| log.write("#{line}")}
  end
  
  def add_line(new_line)
    old = File.new("#{@log}", 'r')
    old_lines = []
    old.each_line {|line| old_lines.push(line)}
    old.close
    new = File.new("#{@log}", 'w')
    new.write("#{Time.now}: #{line}\n")
    new.close
    appender = File.new("#{@log}", 'a')
    old_lines.each {|line| appender.write("#{line}")}
    appender.close
  end
  
  def replace_lines(lines)
    lines = lines.to_a
    lines.each do |line|
      n = lines.index(line)
      self.replace_line(n, line)
    end
  end
end