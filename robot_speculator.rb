# Main script for trading Bitcoin for Litecoin to make profit.
# Buys LTC for half of your BTC if the transaction volume is sufficient.
# Sells half your LTC for BTC when you get more BTC for them than you spent buying.
# Author: Jakob Ledig

require_relative 'cex_api'
require_relative 'transaction'
require 'json'

VERSION = 1.12
# Buying/selling only half of the possible amount thrown out cause it
# blurs the knowlege on how much worht the budget is

#~ @cex = CexAPI.new
@ta = Transaction.new

# Set the last number to your favourite delay for the queries:
@timebase = 60 * 0.5

# Set to true if you want to loop externally (e.g.: with a bash script)
@ext_timekeeping = true


def main_loop
  feedback = false
  @ta.refresh_criteria unless @ext_timekeeping
  reply = @ta.cancel_if_too_old
  if reply == "true"
    log("Success!")
    sleep 10    
  end
  
  # Sell if conditions are met
  if @ta.sell_safe?
      amount = @ta.criteria["btc_available"]
      if @ta.criteria["minimum_btc"] < amount
        price = @ta.criteria["btceur"]
        log "Selling #{amount} BTC for #{price} EUR each, total #{price*amount}"
        response = @ta.sell(amount)
        log_standards(response)
        feedback = true
      else
        log("Can't sell enough BTC. #{@ta.criteria["btc_available"]} < #{@ta.criteria["minimum_btc"]}")
        feedback = true
      end
  end
          
  # Buy if conditions are met:
  if @ta.buy_safe? 
      minimum = @ta.criteria["minimum_btc"]
      min_btc_met = minimum < @ta.criteria["eur_available"] * @ta.criteria["btceur"]
      minimum = @ta.criteria["minimum_eur"]
      min_eur_met = minimum < @ta.criteria["eur_available"]
      price = @ta.criteria["btceur"]
      amount = @ta.criteria["eur_available"]
      if min_btc_met && min_eur_met
        log "Spending #{amount} EUR for #{amount / price} BTC"
        response = @ta.buy(amount)
        log_standards(response)
        feedback = true
      else
        eur = @ta.criteria["eur_available"]
        log("Can't buy enough BTC (#{amount / price}) or spend enough EUR. (#{eur})")
        feedback = true
      end

  end
  
  unless feedback    
    replace_log_line("#{Time.now}: Idle. Balance's worth: #{@ta.sum_up}\n")
  end
end

def log(line)
  old = File.new('log.txt', 'r')
  old_lines = []
  old.each_line {|line| old_lines.push(line)}
  old.close
  new = File.new('log.txt', 'w')
  new.write("#{Time.now}: #{line}\n")
  new.close
  appender = File.new('log.txt', 'a')
  old_lines.each {|line| appender.write("#{line}")}
  appender.close
end

def replace_log_line(line)
  old = File.new('log.txt', 'r')
  lines = []
  old.each_line {|line| lines.push(line)}
  old.close
  lines[0] = line
  log = File.open('log.txt', 'w')  {|file| file.truncate(0)}
  #~ log.close
  log = File.new('log.txt', 'a')
  lines.each {|line| log.write("#{line}")}
end

def log_standards(action)
  log @criteria
  log "Content of account is currently worth #{@ta.sum_up} EUR"
  log action if action
  log "#################################################"
  log ""
end

unless @ext_timekeeping
  main_loop
  rng = @timebase + rand(-10..10)
  sleep (rng)
else
  main_loop
end
