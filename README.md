# ROBOT SPECULATOR

Connects to an account on cex.io, compares prices of cryptocurrencies (standard: Bitcoin and Litecoin) and trades each against another as soon as you get more selling than you spent buying and vice versa. Also, the script checks if the gain is higher than any transaction costs.

Supposed to be resilient against running dry by always only using half of the available budget for speculation. Theoretically, it won't ever act if it can't gain anything. Therefore you're pretty safe against loosing your stake. Except that maybe Bitcoin, Litecoin or another currency that you brought to work with this proves, could prove to be broken of course - or the platform itself breaks.

The software can be run distributed on multiple machines, because it relies on the account data hosted on cex, yet it is not particularly useful to do so because the market doesn't move any faster this way.
Because it greatly benefits from being run 24/7, you might consider using a small, energy-efficient machine like a Raspberry Pi for running the Robot Speculator.

Don't expect becoming rich in a day, but depending on the trade, it could bring in some dividend of maybe 1% per day.

Code quality is still an issue. This is one of my first public projects, please be nice. ^^
## How to use:

1. Install a recent version of [Ruby](https://www.ruby-lang.org)
2. Register at [cex.io](https://cex.io/)
3. Create an API-secret and save it in a text-file called "api_secret.JSON" like this:

	 {"userID": youruserID, "key": yourkey, "secret": yourAPIsecret}
	 
	 On a Unix system, you also might wanna chmod 600 on it so only your user have access to it.
	 
4. Load some cash into your account
5. Once buy some LTC for BTC yourself so there is some data to rely on
6. Donate some of your profit to me :)
    1HPLPUNMgCsdQYbMf8bGwxS59EJbxbYtft

## Roadmap:
* regular payouts to a given (Bitcoin or whatever-) wallet
* generalize to bring the trader into contact with other trading platforms like Kraken etc
* set up an option to leave timekeeping to an external script
* change orders to "instant orders" (cex.io-specific, much faster transactions => more gain)
* enhance logging (ring buffer-like logfile, regularly creating a new file, got some ideas)